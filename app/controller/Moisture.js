Ext.define('APP.controller.Moisture', {
    extend  : 'Ext.app.Controller',

    refs	: [
        {
            ref: 'mpsTree',
            selector: 'moisturepanel #mpsTree'
        },
        {
            ref: 'mpsGraph',
            selector: 'moisturepanel #mpsGraph'
        },
        {
            ref: 'deviceControl',
            selector :'moisturepanel c8ydevicecontrolpanel'
        }
    ],

	init 	: function() {
		this.control({
			'moisturepanel #mpsTree' : {
				selectmo	: this.onSelectMps
			}
		});
	},

	onSelectMps : function(recordMo) {
		var graph = this.getMpsGraph(),
		    deviceControl = this.getDeviceControl();

		graph.loadData({
			source : recordMo.get('id')
		});

		deviceControl.setManagedObject(recordMo);
	}
});