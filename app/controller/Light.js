Ext.define('APP.controller.Light', {
    extend  : 'Ext.app.Controller',

    refs	: [
        {
            ref: 'mpsTree',
            selector: 'lightpanel #mpsTree'
        },
        {
            ref: 'mpsGraph',
            selector: 'lightpanel #mpsGraph'
        },
        {
            ref: 'deviceControl',
            selector :'lightpanel c8ydevicecontrolpanel'
        }
    ],

	init 	: function() {
		this.control({
			'lightpanel #mpsTree' : {
				selectmo	: this.onSelectMps
			}
		});
	},

	onSelectMps : function(recordMo) {
		var graph = this.getMpsGraph(),
		    deviceControl = this.getDeviceControl();

		graph.loadData({
			source : recordMo.get('id')
		});

		deviceControl.setManagedObject(recordMo);
	}
});