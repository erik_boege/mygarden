Ext.define('APP.view.Light', {
    extend : 'Ext.panel.Panel',
    alias   : 'widget.lightpanel',
    requires : [
        'C8Y.ux.InventoryTree',
        'C8Y.ux.MeasurementGraph',
        'C8Y.ux.plugin.Panel',
        'C8Y.ux.DeviceControlPanel'
    ],

    initComponent : function() {
        this.layout = 'border';
        this.bodyStyle="background:transparent;";
        this.items = this.buildItems();
        this.callParent();
    },

    buildItems : function() {
        if (C8Y.client.auth.hasInventoryReadRole() && C8Y.client.auth.hasMeasurementReadRole()) {
            return [
                {
                    xtype               : 'c8yinventorytree',
                    itemId              : 'mpsTree',
                    region              : 'west',
                    width               : 280,
                    childType           : 'childDevices',
                    editable            : false,
                    filterType          : 'Arduino',
                    plugins             : [
                        'c8ypanel'
                    ]
                },
                this.buildGraph()
            ];
        } else {
            Ext.getCmp(this.xtype + 'MenuButton').disable();
            return [];
        }
    },

    buildGraph: function() {
        return {
            xtype    : 'c8ymeasurementgraph',
            itemId   : 'mpsGraph',
            region   : 'center',
            measurementProperty : 'notdefined',
            ytitle      : 'Light (lux)',
            bodyStyle   : 'border-top-width: 0 !important;',
            series  : [
                {
                    type    : 'line',
                    yField  : 'Ev'
                }
            ],
            plugins  : [
                'c8ypanel'
            ]
        };
    },
});