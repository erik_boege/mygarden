Ext.define('APP.view.Footer', {

    extend: 'Ext.container.Container',

    requires: [
               'Ext.layout.container.HBox',
               'Ext.toolbar.Spacer',
               'Ext.toolbar.Separator',
               'Ext.form.Label'
           ],
    
    alias:  'widget.footer',
    
   

    // Overrides eventual configuration passed at instantiation time
    initComponent: function() {
        this.height = 40;
        this.componentCls = 'C8YFooter';
        this.layout = { type : 'hbox', align : 'middle' };
        this.defaults = { xtype : 'button', hreftarget : '_blank' };
        this.items = this.buildItems();
        this.callParent(arguments);
    },

    /**
    * @method
    * Returns the configuration items for the footer
    * @return {Array} items for the footer
    */
    buildItems: function() {
        var btns = this.buildButtons();
        
        return btns.concat( [
            {
                flex  : 1,
                xtype : 'container',
                html  : '&copy; 2013- All rights reserved',
                cls   : 'C8Y-btnsimple-footer',
                margin: '0 10px 0 0'
            }
        ]);
    },

    /**
    * @method
    * Returns the buttons for the footer
    * @return {Array} buttons to be added to the footer
    */
    buildButtons: function() {
        var nbtns = [],
            btns = [
                { text : 'About MyGarden', href : 'http://www.erikboege.blogspot.de/2013/05/helping-our-office-plant-with-arduino-2.html' },
                { text : 'Contact us', href : 'https://plus.google.com/103157208828880220154' }
            ];

        for (var i = 0, t = btns.length; i < t; i++) {
            btns[ i ].cls = 'C8Y-btnsimple-footer';
            if (i === 0) {
                btns[ i ].margin = '0 0 0 10';
            }
            nbtns.push(btns[ i ]);
            if (i < (t - 1)) {
                nbtns.push( {
                    xtype : 'container',
                    html  : '|'
                });
            }
        }

        return nbtns;
    }
});
