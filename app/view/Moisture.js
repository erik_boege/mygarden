Ext.define('APP.view.Moisture', {
    extend : 'Ext.panel.Panel',
    alias   : 'widget.moisturepanel',
    requires : [
        'C8Y.ux.InventoryTree',
        'C8Y.ux.MeasurementGraph',
        'C8Y.ux.plugin.Panel',
        'C8Y.ux.DeviceControlPanel'
    ],

    initComponent : function() {
        this.layout = 'border';
        this.bodyStyle="background:transparent;";
        this.items = this.buildItems();
        this.callParent();
    },

    buildItems : function() {
        if (C8Y.client.auth.hasInventoryReadRole() && C8Y.client.auth.hasMeasurementReadRole()) {
            return [
                {
                    xtype               : 'c8yinventorytree',
                    itemId              : 'mpsTree',
                    region              : 'west',
                    width               : 250,
                    childType           : 'childDevices',
                    editable            : false,
                    filterType          : 'Arduino',
                    plugins             : [
                        'c8ypanel'
                    ]
                },
                this.buildGraph()
            ];
        } else {
            Ext.getCmp(this.xtype + 'MenuButton').disable();
            return [];
        }
    },

    buildGraph: function() {
        return {
            xtype    : 'c8ymeasurementgraph',
            itemId   : 'mpsGraph',
            region   : 'center',
            measurementProperty : 'notdefined',
            ytitle      : 'Moisture (%)/Light (lux)',
            bodyStyle   : 'border-top-width: 0 !important;',
            dockedItems : this.buildControlPanel(),
            series  : [
                {
                    type    : 'line',
                    yField  : 'moisture'
                },
                {
                    type    : 'line',
                    yField  : 'Ev'
                }
            ],
            plugins  : [
                'c8ypanel'
            ]
        };
    },

    buildControlPanel: function() {
        return {
            xtype   : 'c8ydevicecontrolpanel',
            dock    : 'bottom',
            height  : 150,
            title   : 'Device Control',
            controlTypes : this.buildControlTypes()
        };
    },

    buildControlTypes: function() {
        return  [
            {
                id  : '_shallowProperty_toggle',
                name: 'Pump',
                form: [
                    {
                        xtype    : 'combo',
                        store    : Ext.create('Ext.data.Store', {
                            fields: ['Pump'],
                            data : [
                                {"Pump":"ON"},
                                {"Pump":"OFF"}
                            ]
                        }),
                        queryMode   : 'local',
                        displayField: 'Pump',
                        valueField  : 'Pump',
                        editable    : false,
                        fieldLabel  : 'State',
                        name        : 'Pump'
                    }
                ]
            }
        ];
    }
});