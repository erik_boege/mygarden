Ext.require([
'APP.view.Moisture',
'APP.view.Footer',
//'APP.view.Light',
    'C8Y.ux.EventsTabPanel',
]);

C8Y.application( {
    name:        'APP',

    controllers: [
        'C8Y.controller.EventController',
        'C8Y.controller.AuditController',
        'C8Y.controller.AlarmController',
        'Moisture',
        //'Light'        
    ],

    header: 'neutralheader',
    footer: 'footer',

    items:       [
		{
		    xtype:      'moisturepanel',
		    menuOption: 'Moisture',
		    iconCls:    'iconGraph'
		},
//        {
//            xtype:      'lightpanel',
//            menuOption: 'Light',
//            iconCls:    'iconGraph'
//        },
        {
            xtype:      'c8yeventstabpanel',
            itemId:     'c8yeventstabpanel',
            menuOption: 'Events',
            iconCls:    'iconEvents'
        }
    ]
});